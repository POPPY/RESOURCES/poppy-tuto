import os
import os.path as osp
import json

from poppy.core.db.handlers import get_or_create
from poppy.pop.plugins import Plugin
from tuto.texter.models.dictionary import Dictionary
from poppy.core.conf import settings
from poppy.core.logger import logger
from poppy.pop.target import create_target

__all__ = ['decommute', 'load_dict', 'make_text']


MakeTextTask = Plugin.manager['tuto.texter'].task('make_text')


DecomTask = Plugin.manager['tuto.texter'].task('decommute')
@DecomTask.as_task
def decommute(task):
    """
    Replace the word id with the corresponding word for each packets
    """
    list_of_words = list()

    logger.info('In decommute() task')

    # open the input file and load packets
    with task.pipeline.properties.packets.open() as file:
        packets = json.loads(file.readline())

    # get the sqlalchemy session
    session = task.pipeline.db.session

    for packet in packets:
        words = list()

        for id in packet:

            word = get_or_create(
                session,
                Dictionary,
                identifier=id
            ).word

            # in this case, equivalent to :
            # query = session.query(Dictionary)
            # query = query.filter_by(identifier=id)
            # word = query.one().word

            words.append(word)

        list_of_words.append(words)

    target = create_target(task, 'words', target_file='words.txt')

    with target.open("w") as f:
        json.dump(list_of_words, f)


@MakeTextTask.as_task
def make_text(task):
    """
    Create a text from lists of words
    """
    logger.info('In make_text() task')
    sentences = list()

    with task.pipeline.properties.words.open() as file:
        list_of_words = json.loads(file.readline())

    for words in list_of_words:
        sentence = ' '.join(words)

        sentences.append(sentence)

    text = '. '.join(sentences)


    target = create_target(task, 'text', target_file='text.txt')

    with target.open("w") as f:
        json.dump(text, f)




LoadTask = Plugin.manager['tuto.texter'].task('load_dict')

@LoadTask.as_task
def load_dict(task):
    """
    Load the dictionary into the table 'dictionary' of the database
    """

    # get the sqlalchemy session needed to communicate with the database
    session = task.pipeline.db.session

    dictionary = list()

    # build the path to the dictionary file
    dict_file_path = os.path.join(settings.ROOT_DIRECTORY,
                                  'lib',
                                  'dictionary.txt')

    # open the file and build a list element for each line
    with open(dict_file_path, 'r') as file:
        for line in file:
            dictionary.append(line.strip('\n'))

    # create a database entry in the dictionary table for each word
    for i, word in enumerate(dictionary):
        dict_entry = get_or_create(
            session,
            Dictionary,
            identifier=i,
            word=word
        )

        print(f'ID = {dict_entry.identifier} ; word = {dict_entry.word}')



