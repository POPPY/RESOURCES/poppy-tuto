import os
import tempfile
import unittest.mock as mock
from unittest.mock import mock_open

from poppy.core.db.connector import Connector
from poppy.core.test import TaskTestCase, CommandTestCase


class TestTexterTasks(TaskTestCase):

    # mock the MAIN-DB access
    @mock.patch.object(Connector, 'manager')
    def test_make_text(self, mock_manager):
        # --- initialize the task ---
        from tuto.texter.tasks import make_text

        self.task = make_text()

        # --- create some fake data ---
        # (you can use directly pipeline attributes)
        words_data = '[["poppy", "framework", "is", "awesome"],["python", "is", "great"]]'

        self.task.pipeline.properties.words.open = mock_open(read_data=words_data)

        self.task.pipeline.output = tempfile.mkdtemp()

        # --- run the task ---
        self.run_task()

        # --- make assertions ---
        with open(os.path.join(self.task.pipeline.output, 'text.txt'), 'r') as output_file:
            # test the result
            result = (output_file.readlines())
            assert result == ['"poppy framework is awesome. python is great"']


class TestTexterCommands(CommandTestCase):
    @mock.patch("builtins.open", create=True)
    def test_text_from_packet(self, mock_open):
        # --- create some fake data ---


        output = tempfile.mkdtemp()

        packet_filepath = os.path.join(output, 'packets.txt')


        with open(packet_filepath, 'w') as packet_file:
            packet_file.write('[[4, 1, 3, 0], [5, 3, 2]]')

        # --- initialize the command ---

        self.command = ['pop',
                        'texter',
                        'text_from_packet',
                        '--packet_file', packet_filepath,
                        '--output', output]

        # --- run the command ---

        self.run_command()

        # --- make assertions ---

        # test the result
        assert sorted(['cal_text.txt', 'text.txt', 'words.txt']) == sorted(os.listdir(output))

