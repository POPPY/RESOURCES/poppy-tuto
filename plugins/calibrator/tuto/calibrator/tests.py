# from poppy.core.test import TaskTestCase, CommandTestCase
#
#
# class CalibratorTaskTests(TaskTestCase):
#     def test_task1(self):
#         # --- initialize the task ---
#         from tuto.calibrator.tasks import task1
#
#         self.task = task1()
#
#         # --- create some fake data ---
#         # (you can use directly pipeline attributes)
#
#         task.pipeline.properties.input_dir = 'my_input_dir'
#         task.pipeline.properties.output = 'my_output_dir'
#
#         # (...)
#
#         # --- run the task ---
#
#         self.run_task()
#
#         # --- make assertions ---
#
#         # test the result
#         assert task.pipeline.properties.result == 'my_result'
#
#         # (...)
#
#     def test_task2(self):
#         # --- initialize the task ---
#         from tuto.calibrator.tasks import task2
#
#         self.task = task2()
#
#         # (...)
#
# class CalibratorCommandTests(CommandTestCase):
#     def test_sub_command1(self):
#         # --- create some fake data ---
#
#         value1 = 'value1'
#         value2 = 'value2'
#
#         # --- initialize the command ---
#
#         self.command = ['pop',
#                         'calibrator',
#                         'my_sub_command',
#                         '--my_option1', value1,
#                         '--my_option2', value2]
#
#         # --- run the command ---
#
#         self.run_command()
#
#         # --- make assertions ---
#
#         # test the result
#         assert sorted(['file1', 'file2']) == sorted(os.listdir('my_result_path')))
#
#         # (...)
#