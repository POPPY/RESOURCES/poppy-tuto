#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from poppy.pop.models.non_null_column import NonNullColumn
from poppy.core.db.base import Base
from sqlalchemy import String, UniqueConstraint
from sqlalchemy.dialects.postgresql import INTEGER

__all__ = ['Dictionary']


class Dictionary(Base):
    """
    The "dictionary" table contains the dictionary
    """

    id_dictionary = NonNullColumn(INTEGER(), primary_key=True)
    identifier = NonNullColumn(INTEGER(), descr='Identifier of the word', unique=True)
    word = NonNullColumn(String(16), descr='The word', unique=True, comment='Must be an english word.')

    __tablename__ = 'dictionary'
    __table_args__ = (
        UniqueConstraint('identifier', 'word'),
        )