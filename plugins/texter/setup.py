import json
import os

from setuptools import find_packages
from setuptools import setup

ROOT_DIRECTORY = os.path.dirname(os.path.abspath(__file__))

REQ_FILE = os.path.join(ROOT_DIRECTORY, "requirements.txt")


def get_reqs(req_file):
    """Get module dependencies from requirements.txt."""
    if not os.path.isfile(req_file):
        raise BaseException("No requirements.txt file found, aborting!")
    else:
        with open(req_file, 'r') as fr:
            requirements = fr.read().splitlines()

    return requirements

def _get_version():
    """Get version from plugin descriptor"""
    with open('tuto/texter/descriptor.json', 'r') as f:
        descriptor = json.load(f)
    return descriptor['release']['version']

def _get_description():
    """Get description from plugin descriptor"""
    with open('tuto/texter/descriptor.json', 'r') as f:
        descriptor = json.load(f)
    return descriptor['identification']['description']


setup(
    name='tuto.texter',
    version=_get_version(),
    description=_get_description(),
    author="poppy create plugin",
    packages=find_packages(include='tuto/texter'),
    install_requires=get_reqs(REQ_FILE),
    include_package_data=True,
    zip_safe=False,
    package_data={
        "": ["*.json", ]
    }
,
)