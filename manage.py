#!/usr/bin/env python3

import os
import sys

if __name__ == "__main__":

    # run the pipeline
    try:
        from poppy.pop.scripts import main
    except ImportError as exc:
        raise ImportError(
            "Couldn't import poppy-pop. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc


    main()


