
__all__ = ['TexterLoadCommand']

from poppy.core.command import Command
from poppy.pop import Pop
from tuto.texter.tasks import load_dict, decommute, make_text
from tuto.calibrator.tasks import calibrate

class TexterCommand(Command):
    __command__ = "texter"
    __command_name__ = "texter"
    __parent__ = "master"
    __parent_arguments__ = ["base"]
    __help__ = "Commands relative to the texter plugin"

class TexterTextFromPacketCommand(Command):
    """
    Command to reconstruct the original text from the data packets
    """
    __command__ = "texter_text_from_packet"
    __command_name__ = "text_from_packet"
    __parent__ = "texter"
    __parent_arguments__ = ["base"]
    __help__ = """
        Command to reconstruct the original text from the data packets
    """

    def add_arguments(self, parser):
        # path to XML file of the IDB
        parser.add_argument(
            '--packet_file',
            help="""
            The file containing the packets
            """,
            type=str,
        )

    def __call__(self, args):
        # create the pipeline
        pipeline = Pop(args)

        # starting task
        start = decommute()
        end = calibrate()

        # get the class for constructing the Target
        Target = start.input_target("packets")

        # create target instance
        target = Target(target_file=args.packet_file)

        # set it into the pipeline properties
        # (it should have the same name as the input name)
        pipeline.properties.packets = target

        # define the start and end points of the pipeline
        pipeline.start = start
        pipeline.end = end

        # construct the topology
        pipeline | start | make_text() | end

        # run the pipeline
        pipeline.run()

class TexterLoadCommand(Command):
    """
    My job command description
    """
    __command__ = "texter_load"
    __command_name__ = "load"
    __parent__ = "texter"
    __parent_arguments__ = ["base"]
    __help__ = """
        Command to load the dictionary into the database
    """

    def __call__(self, args):
        # create the pipeline
        pipeline = Pop(args)

        # starting task
        start = load_dict()

        pipeline.start = start
        pipeline | start
        pipeline.run()

