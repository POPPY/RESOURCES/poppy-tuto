#!/bin/bash

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER "pipeuser" PASSWORD 'userpwd' NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT;
    CREATE USER "pipeadmin" PASSWORD 'adminpwd' NOSUPERUSER CREATEDB NOCREATEROLE INHERIT;
    CREATE DATABASE docker_pipeline WITH OWNER pipeadmin;
    GRANT CONNECT ON DATABASE docker_pipeline TO pipeuser;
    GRANT ALL PRIVILEGES ON DATABASE docker_pipeline TO pipeadmin;
EOSQL
