See the POPPy framework Documentation for [details](https://poppy-framework.readthedocs.io/en/latest/)

### Set up the POPPy tutorial environment

- Clone this repo
- Create a virtualenv and install requirements

```
$ python3.6 -m venv ~/.virtualenvs/poppy_tuto
$ source ~/.virtualenvs/poppy_tuto/bin/activate
(poppy_tuto)$ cd poppy_tuto
(poppy_tuto)$ pip install -r requirements.txt
```
- Create a postgresql database, for example:

```
$ sudo -u postgres -i
$ psql
postgres=# create user pipeuser with password 'userpwd';
postgres=# create user pipeadmin with password 'adminpwd';
postgres=# create database poppy_tuto with owner pipeadmin;
postgres=# \q
$ exit
```

- Run the migrations

```
(poppy_tuto)$ python manage.py db upgrade heads
```

Now you can run the demo !

### Use the pipeline
- Populate the database with the descriptor

```
(poppy_tuto)$ python manage.py piper descriptor
```

- Populate the database with the dictionary

```
(poppy_tuto)$ python manage.py texter load
```

- Run the reconstruction of the text

```
(poppy_tuto)$ python manage.py texter text_from_packet --packet_file packets.txt
```

### Install and use the pipeline with docker-compose

Prerequisites: install [docker-compose](https://docs.docker.com/compose/install/)

To run the pipeline containers (database and pipeline), open a terminal, move to the pipeline root dir (`cd path/to/poppy_tuto`) and type `docker-compose up`. The containers should start.

To open an interactive shell inside the pipeline container, open a second terminal and type `docker-compose exec pipeline bash`.

Note that:

- you can also run pipelines commands directly from the host machine using `docker-compose exec pipeline python manage.py`.
- you have to run manually the configuration script to install the pipeline plugins and set up the database: `docker-compose exec pipeline ./scripts/configure-pipeline.sh` 

To clean the containers, stop the execution of the pipeline and the database (press CTRL-C in the terminal where the containers are executed and wait for containers stopping properly). Then simply use `docker-compose down`.
